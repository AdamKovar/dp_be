FROM openjdk:8-jdk-alpine
ADD target/docker-ais-api.jar app.jar
ENTRYPOINT ["java","-jar","/app.jar"]