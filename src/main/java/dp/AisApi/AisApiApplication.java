package dp.AisApi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;

@SpringBootApplication
@EnableMongoRepositories(basePackages = {"dp.AisApi.Repository"})
@EnableSwagger2
@ComponentScan(basePackages = {"dp.AisApi.*"})
public class AisApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(AisApiApplication.class, args);
    }

    @Bean
    public Docket swaggerConfiguration() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .build()
                .apiInfo(apiDetails());
    }

    private ApiInfo apiDetails() {
        return new ApiInfo(
                "AIS information API",
                "Basic API for PocketAIS Android application",
                "v 1.0",
                "Free to use",
                new Contact("Adam Kovar", "", "kovar.adams@gmail.com"),
                "API license",
                "Url to license",
                Collections.emptyList());
    }

}
