package dp.AisApi.Controller;

import dp.AisApi.Model.Faculty.FacultyContact;
import dp.AisApi.Model.Faculty.FacultyContactPerson;
import dp.AisApi.Model.Faculty.FacultyOpenHour;
import dp.AisApi.Model.Faculty.FacultyType;
import dp.AisApi.Service.FacultyContactService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/rest/api/faculty")
public class FacultyInfoController {

    Logger logger = LoggerFactory.getLogger(SecurityController.class);

    @Autowired
    private FacultyContactService facultyContactService;

    @GetMapping("/info")
    public ResponseEntity<FacultyContact> getFacultyInfo(@RequestParam(value = "login") String login,
                                                         @RequestParam(value = "faculty") Integer facultyId) {
        FacultyContact contact = facultyContactService.getFacultyContact(login, facultyId);
        if(contact != null){
            return new ResponseEntity<>(contact, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/info/all")
    public ResponseEntity<List<FacultyContact>> getAllFacultiesInfo(@RequestParam(value = "login") String login) {
        List<FacultyContact> contact = facultyContactService.getContactsForAllFaculties(login);
        if(contact != null){
            return new ResponseEntity<>(contact, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/openHours")
    public ResponseEntity<List<FacultyOpenHour>> getOpenHours(@RequestParam(value = "login") String login,
                                                                 @RequestParam(value = "faculty") Integer facultyId) {
        List<FacultyOpenHour> openHours = facultyContactService.getOpeningHoursForFaculty(login, facultyId);
        if(!openHours.isEmpty()) {
            return new ResponseEntity<>(openHours, HttpStatus.OK);
        }
        return new ResponseEntity<>(new ArrayList<>(), HttpStatus.NOT_FOUND);
    }

    @GetMapping("/openHours/all")
    public ResponseEntity<List<FacultyOpenHour>> getOpenHoursForAllFaculties(@RequestParam(value = "login") String login) {
        List<FacultyOpenHour> openHours = facultyContactService.getOpeningHoursForAllFaculties(login);
        if(!openHours.isEmpty()) {
            return new ResponseEntity<>(openHours, HttpStatus.OK);
        }
        return new ResponseEntity<>(new ArrayList<>(), HttpStatus.NOT_FOUND);
    }

    @GetMapping("/contactPeople")
    public ResponseEntity<List<FacultyContactPerson>> getContactPeople(@RequestParam(value = "login") String login,
                                                              @RequestParam(value = "faculty") Integer facultyId) {
        List<FacultyContactPerson> contactPeople = facultyContactService.getFacultyContactPeople(login, facultyId);
        if(!contactPeople.isEmpty()) {
            return new ResponseEntity<>(contactPeople, HttpStatus.OK);
        }
        return new ResponseEntity<>(new ArrayList<>(), HttpStatus.NOT_FOUND);
    }

    @GetMapping("/contactPeople/all")
    public ResponseEntity<List<FacultyContactPerson>> getContactPeopleForAllFaculties(@RequestParam(value = "login") String login) {
        List<FacultyContactPerson> contactPeople = facultyContactService.getFacultyContactPeopleForAllFaculties(login);
        if(!contactPeople.isEmpty()) {
            return new ResponseEntity<>(contactPeople, HttpStatus.OK);
        }
        return new ResponseEntity<>(new ArrayList<>(), HttpStatus.NOT_FOUND);
    }

    @GetMapping("/contactViceDean")
    public ResponseEntity<List<FacultyContactPerson>> getContactViceDeans(@RequestParam(value = "login") String login,
                                                                       @RequestParam(value = "faculty") Integer facultyId) {
        List<FacultyContactPerson> contactPeople = facultyContactService.getFacultyContactViceDeans(login, facultyId);
        if(contactPeople != null && !contactPeople.isEmpty()) {
            return new ResponseEntity<>(contactPeople, HttpStatus.OK);
        }
        return new ResponseEntity<>(new ArrayList<>(), HttpStatus.NOT_FOUND);
    }

    @GetMapping("/contactViceDean/all")
    public ResponseEntity<List<FacultyContactPerson>> getContactViceDeans(@RequestParam(value = "login") String login) {
        List<FacultyContactPerson> contactPeople = facultyContactService.getFacultyContactViceDeansForAllFaculties(login);
        if(!contactPeople.isEmpty()) {
            return new ResponseEntity<>(contactPeople, HttpStatus.OK);
        }
        return new ResponseEntity<>(new ArrayList<>(), HttpStatus.NOT_FOUND);
    }

    @GetMapping("/allFaculties")
    public ResponseEntity<List<FacultyType>> getAllFaculties() {
        return new ResponseEntity<>(Arrays.asList(FacultyType.values()), HttpStatus.OK);
    }




}
