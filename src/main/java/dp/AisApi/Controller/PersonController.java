package dp.AisApi.Controller;

import dp.AisApi.Model.Email.EmailMessage;
import dp.AisApi.Model.Email.EmailPreview;
import dp.AisApi.Model.Email.InboxType;
import dp.AisApi.Model.Email.MailBoxStatus;
import dp.AisApi.Model.Person.Person;
import dp.AisApi.Model.Person.PersonInfo;
import dp.AisApi.Service.PersonService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/rest/api/person")
public class PersonController {

    Logger logger = LoggerFactory.getLogger(PersonController.class);

    @Autowired
    private PersonService personService;

    @GetMapping("/info")
    @ApiOperation(value = "Get person info, by AIS login",
            notes = "",
            response = Person.class)
    public ResponseEntity<Person> getPersonalInfo(@ApiParam(value = "AIS login", required = true) @RequestParam(value = "login") String login) {
        logger.info("Try to get personal data for user with login: {}", login);
        Person person = personService.getPersonalInfo(login);
        return new ResponseEntity<>(person, HttpStatus.OK);
    }

    @GetMapping("/allInfo")
    public ResponseEntity<List<PersonInfo>> getAllPersonalInfo(@ApiParam(value = "AIS login", required = true) @RequestParam(value = "login") String login) {
        logger.info("Try to get personal data for user with login: {}", login);
        List<PersonInfo> personalInfo = personService.getAllPersonalInfo(login);
        return new ResponseEntity<>(personalInfo, HttpStatus.OK);
    }

    @GetMapping("/picture/{aisId}")
    @ResponseBody
    public HttpEntity<byte[]> getArticleImage(@PathVariable String aisId,
                                              @RequestParam(value = "login") String login) {

        logger.info("Requested picture >> " + aisId + " <<");

        // 1. download img from http://internal-picture-db/id.jpg ...
        byte[] image = personService.getImage(login, aisId);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.IMAGE_JPEG);
        headers.setContentLength(image.length);

        return new HttpEntity<byte[]>(image, headers);
    }

    @RequestMapping("/mail/all")
    @ResponseBody
    public ResponseEntity<List<EmailPreview>> getMails(@RequestParam(value = "login") String login,
                                                       @RequestParam(value = "inboxType") Integer id) {
        InboxType inboxType = InboxType.findById(id);
        List<EmailPreview> emailPreviewList = personService.getMailPreview(login, inboxType);
        return new ResponseEntity<List<EmailPreview>>(emailPreviewList, HttpStatus.OK);
    }

    @RequestMapping("/mail/{startIndex}/{endIndex}")
    @ResponseBody
    public ResponseEntity<List<EmailPreview>> getMails(@RequestParam(value = "login") String login,
                                                       @RequestParam(value = "inboxType") Integer id,
                                                       @PathVariable Integer startIndex,
                                                       @PathVariable Integer endIndex) {
        InboxType inboxType = InboxType.findById(id);
        List<EmailPreview> emailPreviewList = personService.getMailPreview(login, inboxType, startIndex, endIndex);
        return new ResponseEntity<List<EmailPreview>>(emailPreviewList, HttpStatus.OK);
    }


    @RequestMapping("/mail/{mailId}")
    @ResponseBody
    public ResponseEntity<EmailMessage> getMails(@RequestParam(value = "login") String login,
                                                 @RequestParam(value = "inboxType") Integer id,
                                                 @PathVariable Integer mailId) {
        InboxType inboxType = InboxType.findById(id);
        EmailMessage email = personService.getEmail(login, mailId, inboxType);
        return new ResponseEntity<EmailMessage>(email, HttpStatus.OK);
    }

    @RequestMapping("/mail/status")
    @ResponseBody
    public ResponseEntity<MailBoxStatus> getMailBoxStatus(@RequestParam(value = "login") String login,
                                                          @RequestParam(value = "inboxType") Integer id) {
        InboxType inboxType = InboxType.findById(id);
        MailBoxStatus status = personService.getMailBoxStatus(login, inboxType);
        return new ResponseEntity<MailBoxStatus>(status, HttpStatus.OK);
    }

    @RequestMapping("/mail/{mailId}/markAsRead")
    @ResponseBody
    public void markAsRead(@RequestParam(value = "login") String login,
                           @RequestParam(value = "inboxType") Integer id,
                           @PathVariable Integer mailId) {
        InboxType inboxType = InboxType.findById(id);
        personService.markEmailAsRead(login, mailId, inboxType);
    }

}
