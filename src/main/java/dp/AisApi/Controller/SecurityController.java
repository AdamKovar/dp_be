package dp.AisApi.Controller;

import dp.AisApi.Service.SecurityService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/rest/api")
public class SecurityController {

    Logger logger = LoggerFactory.getLogger(SecurityController.class);

    private static final String AUTH_SUCCESS = "Authentication SUCCESS!";
    private static final String AUTH_FAILED = "Authentication FAILED!";

    @Autowired
    private SecurityService securityService;

    @GetMapping("/login")
    public ResponseEntity<String> logIn(@RequestParam(value = "login") String login,
                                        @RequestParam(value = "password") String password) {
        logger.info("Try to log in user with login: {}", login);
        Boolean result = securityService.logIn(login, password);
        if(result){
            return new ResponseEntity<>(AUTH_SUCCESS,HttpStatus.OK);
        } else {
            return new ResponseEntity<>(AUTH_FAILED,HttpStatus.NOT_FOUND);
        }
    }

}
