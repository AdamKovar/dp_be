package dp.AisApi.Controller;

import dp.AisApi.Model.Course.Course;
import dp.AisApi.Service.HeaderUtils;
import dp.AisApi.Service.StudyInfoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.ByteArrayInputStream;
import java.util.List;

@RestController
@RequestMapping("/rest/api/study")
public class StudyInfoController {

    Logger logger = LoggerFactory.getLogger(StudyInfoController.class);

    @Autowired
    StudyInfoService studyInfoService;

    @GetMapping("/info")
    public ResponseEntity<List<Course>> studyInfo(@RequestParam(value = "login") String login) {
        return new ResponseEntity<>(studyInfoService.getStudyInfo(login),HttpStatus.OK);
    }

    @GetMapping("/pdf")
    public ResponseEntity<byte[]> getPDF(@RequestParam(value = "login") String login,
                                         @RequestParam(value = "docType") String docType) {
        byte[] data = studyInfoService.getPdf(login, docType);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_PDF);
        // Here you have to set the actual filename of your pdf
        String filename = "file.pdf";
        headers.setContentDispositionFormData(filename, filename);
        headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
        ResponseEntity<byte[]> response = new ResponseEntity<>(data, headers, HttpStatus.OK);
        return response;
    }

}
