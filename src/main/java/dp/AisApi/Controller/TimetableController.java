package dp.AisApi.Controller;

import dp.AisApi.Model.Event.TimeTableEvent;
import dp.AisApi.Service.TimeTableService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/rest/api/timetable")
public class TimetableController {

    Logger logger = LoggerFactory.getLogger(TimetableController.class);

    @Autowired
    private TimeTableService timeTableService;

    @GetMapping
    public ResponseEntity<List<TimeTableEvent>> getTimetable(@RequestParam(value = "login") String login){
        return new ResponseEntity<>(timeTableService.getTimeTable(login), HttpStatus.OK);
    }
}
