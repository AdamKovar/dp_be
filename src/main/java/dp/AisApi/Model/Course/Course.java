package dp.AisApi.Model.Course;

import dp.AisApi.Model.SemesterType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;

@Getter
@Setter
@AllArgsConstructor
public class Course {

    @Id
    private String _id;

    private String name;
    private String reference;
    private String courseNumber;
    private CourseType type;
    private String language;
    private ExamType examType;
    private String examAttempt;
    private ExamMark examMark;
    private String credits;
    private SemesterType semesterType;
    private String period;
    private String faculty;
    private String login;

    public Course(String name, String reference, String courseNumber, CourseType type, String language,
                  ExamType examType, String examAttempt, ExamMark examMark, String credits, SemesterType semesterType,
                  String period, String faculty, String login) {
        this.name = name;
        this.reference = reference;
        this.courseNumber = courseNumber;
        this.type = type;
        this.language = language;
        this.examType = examType;
        this.examAttempt = examAttempt;
        this.examMark = examMark;
        this.credits = credits;
        this.semesterType = semesterType;
        this.period = period;
        this.faculty = faculty;
        this.login = login;
    }

    public Course update(Course course, ExamMark examMark, String examAttempt) {
        course.setExamMark(examMark);
        course.setExamAttempt(examAttempt);
        return course;
    }

    public Course() {
    }
}
