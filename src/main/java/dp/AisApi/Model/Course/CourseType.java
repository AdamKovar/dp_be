package dp.AisApi.Model.Course;

public enum CourseType {

    COURSE_TYPE_P(0, "Povinný ", "Compulsory"),
    COURSE_TYPE_PV(1, "Povinne voliteľný", "Semi-compulsory"),
    COURSE_TYPE_V(2, "Výberový", "Optional");

    private int id;

    private String nameSK;

    private String nameEN;

    CourseType(int id, String nameSK, String nameEN) {
        this.id = id;
        this.nameSK = nameSK;
        this.nameEN = nameEN;
    }

    public static CourseType getByType(String type) {
        return CourseType.valueOf("COURSE_TYPE_".concat(type.toUpperCase()));
    }
}
