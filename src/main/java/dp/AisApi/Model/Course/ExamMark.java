package dp.AisApi.Model.Course;

public enum ExamMark {

    EXAM_MARK_A(0, "Výborne", "Excellent"),
    EXAM_MARK_B(1, "Veľmi dobre", "Very good"),
    EXAM_MARK_C(2, "Dobre", "Good"),
    EXAM_MARK_D(3, "Uspokojivo", "Laudable"),
    EXAM_MARK_E(4, "Dostatočne", "Satisfactory"),
    EXAM_MARK_FX(5, "Nedostatočne", "Failed"),
    EXAM_MARK_FN(6, "Neabsolvoval", "Not graduate"),
    EXAM_MARK_NONE(7, "", "");

    private int id;

    private String nameSK;

    private String nameEN;

    ExamMark(int id, String nameSK, String nameEN) {
        this.id = id;
        this.nameSK = nameSK;
        this.nameEN = nameEN;
    }

    public static ExamMark getMark(String mark) {
       return ExamMark.valueOf("EXAM_MARK_".concat(mark.substring(mark.indexOf('(')+1,mark.indexOf(')'))));
    }
}
