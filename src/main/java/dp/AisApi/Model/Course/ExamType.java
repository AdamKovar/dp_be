package dp.AisApi.Model.Course;

public enum ExamType {

    EXAM_TYPE_EXM(0, "", ""),
    EXAM_TYPE_CLASS_REQ(1, "", ""),
    EXAM_TYPE_REQ(2, "", "");


    private int id;

    private String nameSK;

    private String nameEN;

    ExamType(int id, String nameSK, String nameEN) {
        this.id = id;
        this.nameSK = nameSK;
        this.nameEN = nameEN;
    }

    public static ExamType getByName(String name) {
        if(name.equals("s")) {
            return EXAM_TYPE_EXM;
        } else if(name.equals("kz")) {
            return EXAM_TYPE_CLASS_REQ;
        } else if (name.equals("z")) {
            return EXAM_TYPE_REQ;
        } else {
            return ExamType.valueOf("EXAM_TYPE_".concat(name.toUpperCase().replace(" ", "_")));
        }
    }

}
