package dp.AisApi.Model;

public enum DayOfWeek {

    MONDAY(0,1,"Mon", "Po","Pondelok"),
    TUESDAY(1,2,"Tue","Ut","Utorok"),
    WEDNESDAY(2,3, "Wed","St","Streda"),
    THURSDAY(3,4, "Thu","Št","Štvrtok"),
    FRIDAY(4,5, "Fri","Pi","Piatok"),
    SATURDAY(5,6, "Sat","So","Sobota"),
    SUNDAY(6,7, "San","Ne","Nedeľa");

    private Integer id;
    private Integer noInWeek;
    private String label;
    private String skLabel;
    private String name;

    DayOfWeek(Integer id, Integer noInWeek, String label, String skLabel, String name) {
        this.id = id;
        this.noInWeek = noInWeek;
        this.label = label;
        this.skLabel = skLabel;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public Integer getNoInWeek() {
        return noInWeek;
    }

    public String getName() {
        return name;
    }

    public static DayOfWeek geDayOfWeekById(Integer id) {
        for(DayOfWeek day : DayOfWeek.values()){
            if(day.id == id) {
                return day;
            }
        }
        return null;
    }

    public static DayOfWeek geDayOfWeekByLabel(String label) {
        for(DayOfWeek day : DayOfWeek.values()){
            if(day.label.toLowerCase().equals(label.toLowerCase())) {
                return day;
            }
        }
        for(DayOfWeek day : DayOfWeek.values()){
            if(day.skLabel.toLowerCase().equals(label.toLowerCase())) {
                return day;
            }
        }
        return null;
    }
}
