package dp.AisApi.Model.Document;

import lombok.Getter;

@Getter
public enum DocumentType {

    STUDY_CONFIRMATION(0, "Study confirmation", "https://is.stuba.sk/auth/student/potvrzeni_o_studiu.pl?" +
            "studium=#STUDY_NO#&obdobi=#PERIOD_NO#&login_hidden=1&auth_id_hidden=0&credential_0=#LOGIN#" +
            "&credential_1=#PASSWORD#&tisk=Tlačiťpotvrdenie"),
    STUDY_CONFIRMATION_EN(1, "Study confirmation EN", "https://is.stuba.sk/auth/student/potvrzeni_o_studiu.pl?" +
            "studium=#STUDY_NO#&obdobi=#PERIOD_NO#&login_hidden=1&auth_id_hidden=0&credential_0=#LOGIN#" +
            "&credential_1=#PASSWORD#&tisk=Tlačiťpotvrdenie&jazyk=eng"),
    REGISTRATION_SHEET(2, "Registration sheet", "http://is.stuba.sk/auth/student/moje_studium.pl?" +
            "reg_arch_tisk=1;studium=#STUDY_NO#;obdobi=#PERIOD_NO#&login_hidden=1&auth_id_hidden=0" +
            "&credential_0=#LOGIN#&credential_1=#PASSWORD#"),
    ENROLLMENT_SHEET(3, "Enrollment sheet", "");


    private int id;

    private String name;

    private String url;

    DocumentType(int id, String name, String url) {
        this.id = id;
        this.name = name;
        this.url = url;
    }
}
