package dp.AisApi.Model.Email;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class EmailConfiguration {

    private String protocol;
    private String host;
    private String port;
    private String userName;
    private String password;

    public EmailConfiguration() {
    }

    public static EmailConfiguration from(String userName, String password, InboxType inboxType) {
        EmailConfiguration configuration = new EmailConfiguration();
        configuration.setProtocol(inboxType.getProtocol());
        configuration.setHost(inboxType.getHost());
        configuration.setPort(inboxType.getPort());
        configuration.setUserName(userName);
        configuration.setPassword(password);
        return configuration;
    }

}
