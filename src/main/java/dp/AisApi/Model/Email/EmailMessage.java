package dp.AisApi.Model.Email;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class EmailMessage {

    private String from;

    private String to;

    private String cc;

    private String subject;

    private String sentDate;

    private String sentTime;

    private String message;

    public EmailMessage() {
    }
}
