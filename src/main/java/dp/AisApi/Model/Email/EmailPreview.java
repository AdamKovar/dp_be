package dp.AisApi.Model.Email;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class EmailPreview {

    private Integer id;

    private String from;

    private String fromMail;

    private String sentDate;

    private String sentTime;

    private String title;

    private String status;

    public EmailPreview() {
    }
}
