package dp.AisApi.Model.Email;

import lombok.Getter;

@Getter
public enum InboxType {

    IS_STUBA(1, "pop3","mail.is.stuba.sk","995"),
    STUBA(2,"imap","imap.stuba.sk","993");

    private Integer id;
    private String protocol;
    private String host;
    private String port;

    InboxType(Integer id, String protocol, String host, String port) {
        this.id = id;
        this.protocol = protocol;
        this.host = host;
        this.port = port;
    }

    public static InboxType findById(Integer id) {
        for(InboxType inboxType : InboxType.values()) {
            if(inboxType.id.compareTo(id) == 0) {
                return inboxType;
            }
        }
        return null;
    }
}
