package dp.AisApi.Model.Email;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class MailBoxStatus {

    private Integer allMessages;

    private Integer unreadMessages;

    public MailBoxStatus() {
    }
}
