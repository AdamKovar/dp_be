package dp.AisApi.Model.Event;

import dp.AisApi.Model.DayOfWeek;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
public class TimeTableEvent implements Serializable {

    @Id
    private String _id;

    private DayOfWeek day;
    private String name;
    private String courseNo;
    private String start;
    private String end;
    private String room;
    private String teacher;
    private String eventType;
    private String login;
    private String validFrom;
    private String validTo;



    public TimeTableEvent() {
    }

}
