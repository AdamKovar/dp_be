package dp.AisApi.Model.Faculty;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
public class FacultyContact implements Serializable {

    @Id
    private String _id;
    private FacultyType faculty;
    private String name;
    private String address;
    private String phone;
    private String fax;
    private String mail;
    private String note;
    @LastModifiedDate
    private Date lastModifiedDate;

    public FacultyContact() {}
}
