package dp.AisApi.Model.Faculty;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;

import java.io.Serializable;

@Getter
@Setter
public class FacultyContactPerson implements Serializable {

    @Id
    private String _id;
    private FacultyType faculty;
    private FacultyPersonType personType;
    private String aisId;
    private String name;
    private String room;
    private String phone;
    private String mail;
    private String note;

    public FacultyContactPerson() { }

}

