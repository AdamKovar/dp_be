package dp.AisApi.Model.Faculty;

import dp.AisApi.Model.DayOfWeek;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;

import java.io.Serializable;

@Getter
@Setter
public class FacultyOpenHour implements Serializable {

    @Id
    private String _id;
    private FacultyType faculty;
    private DayOfWeek dayOfWeek;
    private String from;
    private String to;
    private String note;

    public FacultyOpenHour(){}

}
