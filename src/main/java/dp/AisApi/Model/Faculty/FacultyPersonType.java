package dp.AisApi.Model.Faculty;

public enum FacultyPersonType {
    CONTACT_PERSON(0,"Kontaktná osoba","Contact Person"),
    VICE_DEAN(1, "Prodekan", "Vice-dean");

    private Integer id;
    private String nameSK;
    private String nameEN;

    FacultyPersonType(Integer id, String nameSK, String nameEN) {
        this.id = id;
        this.nameSK = nameSK;
        this.nameEN = nameEN;
    }

    public Integer getId() {
        return id;
    }

    public String getNameSK() {
        return nameSK;
    }

    public String getNameEN() {
        return nameEN;
    }
}
