package dp.AisApi.Model.Person;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;

import java.io.Serializable;

@ApiModel(description = "Basic information about person")
@Getter
@Setter
public class Person implements Serializable {

    @Id
    @ApiModelProperty(notes = "The unique id of the person")
    private String _id;
    @ApiModelProperty(notes = "")
    private String name;
    @ApiModelProperty(notes = "")
    private String aisId;
    @ApiModelProperty(notes = "")
    private String login;
    @ApiModelProperty(notes = "")
    private String title;
    @ApiModelProperty(notes = "")
    private String dateOfBirth;
    @ApiModelProperty(notes = "")
    private String gender;
    @ApiModelProperty(notes = "")
    private String mail;

    public Person(String name, String aisId, String login, String title, String dateOfBirth, String sex, String mail) {
        this.name = name;
        this.aisId = aisId;
        this.login = login;
        this.title = title;
        this.dateOfBirth = dateOfBirth;
        this.gender = sex;
        this.mail = mail;
    }

    public Person() {
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", aisId='" + aisId + '\'' +
                '}';
    }

    public void update (Person person) {
        this.dateOfBirth = person.dateOfBirth;
        this.gender = person.gender;
        this.mail = person.mail;
        this.name = person.name;
        this.title = person.title;
    }
}
