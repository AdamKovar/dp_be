package dp.AisApi.Model;

public enum SemesterType {

    SEMESTER_TYPE_SS(0, "LS","Letný", "Summer"),
    SEMESTER_TYPE_WS(1, "ZS","Zimný", "Winter");

    private int id;

    private String acronymSk;
    private String nameSK;

    private String nameEN;

    SemesterType(int id, String acronymSk,String nameSK, String nameEN) {
        this.id = id;
        this.nameSK = nameSK;
        this.acronymSk = acronymSk;
        this.nameEN = nameEN;
    }

    public SemesterType getSemesterType(String type) {
        return SemesterType.valueOf("SEMESTER_TYPE_".concat(type.toUpperCase()));
    }

    public static SemesterType findByAcronym(String ac) {
        for(SemesterType semesterType : SemesterType.values()) {
            if(semesterType.name().contains(ac)) {
                return semesterType;
            }
        }
        for(SemesterType semesterType : SemesterType.values()) {
            if(semesterType.acronymSk.equalsIgnoreCase(ac)) {
                return semesterType;
            }
        }
        return null;
    }
}
