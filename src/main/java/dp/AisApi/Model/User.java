package dp.AisApi.Model;

import dp.AisApi.Model.Faculty.FacultyType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;

import java.io.Serializable;
import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
public class User implements Serializable {

    @Id
    private String _id;
    private String login;
    private String password;
    private String uis_auth;
    private String aisId;
    private FacultyType facultyType;
    private String currentPeriod;
    private String studyNumber;

    private LocalDateTime lastUpdateTime;

    public User(String login, String uis_auth, String aisId) {
        this.login = login;
        this.uis_auth = uis_auth;
        this.aisId = aisId;
    }

    public User() {
    }


}
