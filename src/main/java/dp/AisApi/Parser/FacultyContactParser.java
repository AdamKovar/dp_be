package dp.AisApi.Parser;

import dp.AisApi.Model.DayOfWeek;
import dp.AisApi.Model.Faculty.*;
import dp.AisApi.Model.User;
import dp.AisApi.Repository.FacultyContactPersonRepository;
import dp.AisApi.Repository.FacultyContactRepository;
import dp.AisApi.Repository.FacultyOpenHourRepository;
import dp.AisApi.Service.FacultyContactService;
import dp.AisApi.Service.UserService;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Node;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class FacultyContactParser {

    @Autowired
    private UserService userService;

    @Autowired
    private FacultyContactRepository facultyContactRepository;

    @Autowired
    private FacultyOpenHourRepository facultyOpenHourRepository;

    @Autowired
    private FacultyContactPersonRepository facultyContactPersonRepository;

    Logger logger = LoggerFactory.getLogger(FacultyContactService.class);


    public FacultyContact getFacultyContact(String login, Integer facultyId) {
        Document doc;
        User user = userService.getUserByLogin(login);
        try {
            doc = Jsoup.connect("https://is.stuba.sk/auth/pracoviste/stud_odd.pl")
                    .data("fakulta", facultyId.toString())
                    .cookie("UISAuth", user.getUis_auth())
                    .get();
            logger.info(doc.title());
            List<Node> nodes = doc.select("table").get(0).childNode(1).childNodes();
            FacultyContact contact = new FacultyContact();
            contact.setFaculty(FacultyType.findById(facultyId));
            for (int i = 0; i < nodes.size(); i++) {
                String value = nodes.get(i).childNode(1).childNode(0).childNode(0).toString().replace("\n", "");
                if (nodes.get(i).childNode(0).toString().contains("Type of department")) {
                    contact.setName(value.replace("&nbsp;", " "));
                } else if (nodes.get(i).childNode(0).toString().contains("Contact address")) {
                    contact.setAddress(value.replace("&nbsp;", " "));
                } else if (nodes.get(i).childNode(0).toString().contains("Phone")) {
                    contact.setPhone(value.replace("&nbsp;", " "));
                } else if (nodes.get(i).childNode(0).toString().contains("Fax")) {
                    contact.setFax(value.replace("&nbsp;", " "));
                } else if (nodes.get(i).childNode(0).toString().contains("E-mail")) {
                    contact.setMail(value.replace("&nbsp;", " "));
                } else if (nodes.get(i).childNode(0).toString().contains("Note")) {
                    contact.setNote(value.replace("&nbsp;", " "));
                } else {
                }
            }
            FacultyContact savedContact = facultyContactRepository.findByFaculty(FacultyType.findById(facultyId));
            if (savedContact != null) {
                facultyContactRepository.delete(savedContact);
            }
            facultyContactRepository.save(contact);
            return facultyContactRepository.findByFaculty(FacultyType.findById(facultyId));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


    public List<FacultyContactPerson> getFacultyContactPeople(String login, Integer facultyId) {
        Document doc;
        User user = userService.getUserByLogin(login);
        try {
            doc = Jsoup.connect("https://is.stuba.sk/auth/pracoviste/stud_odd.pl")
                    .data("fakulta", facultyId.toString())
                    .cookie("UISAuth", user.getUis_auth())
                    .get();
            logger.info(doc.title());
            List<Node> nodes = doc.select("table").get(2).childNode(1).childNodes();
            List<FacultyContactPerson> contactPeople = new ArrayList<>();
            for (int i = 0; i < nodes.size(); i++) {
                FacultyContactPerson contactPerson = new FacultyContactPerson();
                contactPerson.setFaculty(FacultyType.findById(facultyId));
                List<Node> nodesInRow = nodes.get(i).childNodes();
                for (int j = 0; j < nodesInRow.size(); j++) {
                    Node currentNode = null;
                    try {
                        currentNode = nodesInRow.get(j).childNode(0).childNode(0);
                    } catch (Exception e) {

                    }
                    if (currentNode != null) {
                        String value = currentNode.toString().replace("\n", "");
                        contactPerson.setPersonType(FacultyPersonType.CONTACT_PERSON);
                        if (j == 0) {
                            contactPerson.setAisId(nodesInRow.get(j).childNode(0).childNode(0).attr("href").split("id=")[1].replace("\n", ""));
                            contactPerson.setName(nodesInRow.get(j).childNode(0).childNode(0).childNode(0).toString().replace("\n", ""));
                        } else if (j == 1) {
                            contactPerson.setRoom(value.replace("&nbsp;", " "));
                        } else if (j == 2) {
                            contactPerson.setPhone(value.replace("&nbsp;", " "));
                        } else if (j == 3) {
                            contactPerson.setMail(value.replace("&nbsp;", " "));
                        } else if (j == 5) {
                            contactPerson.setNote(value.replace("&nbsp;", " "));
                        } else {
                        }
                    }

                }
                contactPeople.add(contactPerson);
            }
            if (!contactPeople.isEmpty()) {
                List<FacultyContactPerson> savedFacultyContactPerson = facultyContactPersonRepository.findAllByFaculty(FacultyType.findById(facultyId));
                facultyContactPersonRepository.deleteAll(savedFacultyContactPerson);
            }
            facultyContactPersonRepository.saveAll(contactPeople);
            return facultyContactPersonRepository.findAllByFaculty(FacultyType.findById(facultyId));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


    public List<FacultyContactPerson> getFacultyContactViceDeans(String login, Integer facultyId) {
        Document doc;
        User user = userService.getUserByLogin(login);
        try {
            doc = Jsoup.connect("https://is.stuba.sk/auth/pracoviste/stud_odd.pl")
                    .data("fakulta", facultyId.toString())
                    .cookie("UISAuth", user.getUis_auth())
                    .get();
            logger.info(doc.title());
            List<Node> nodes;
            try{
                nodes = doc.select("table").get(3).childNode(1).childNodes();
            } catch (Exception e) {
                return null;
            }

            List<FacultyContactPerson> contactPeople = new ArrayList<>();
            for (int i = 0; i < nodes.size(); i++) {
                FacultyContactPerson contactPerson = new FacultyContactPerson();
                contactPerson.setFaculty(FacultyType.findById(facultyId));
                List<Node> nodesInRow = nodes.get(i).childNodes();
                for (int j = 0; j < nodesInRow.size(); j++) {
                    Node currentNode = null;
                    try {
                        currentNode = nodesInRow.get(j).childNode(0).childNode(0);
                    } catch (Exception e) {

                    }
                    if (currentNode != null) {
                        String value = currentNode.toString().replace("\n", "");
                        contactPerson.setPersonType(FacultyPersonType.VICE_DEAN);
                        if (j == 0) {
                            contactPerson.setAisId(nodesInRow.get(j).childNode(0).childNode(0).attr("href").split("id=")[1].replace("\n", ""));
                            contactPerson.setName(nodesInRow.get(j).childNode(0).childNode(0).childNode(0).toString().replace("\n", ""));
                        } else if (j == 1) {
                            contactPerson.setRoom(value.replace("&nbsp;", " "));
                        } else if (j == 2) {
                            contactPerson.setPhone(value.replace("&nbsp;", " "));
                        } else if (j == 3) {
                            contactPerson.setMail(value.replace("&nbsp;", " "));
                        } else if (j == 5) {
                            contactPerson.setNote(value.replace("&nbsp;", " "));
                        } else {
                        }
                    }
                }
                contactPeople.add(contactPerson);
            }
            if (!contactPeople.isEmpty()) {
                List<FacultyContactPerson> savedFacultyContactPerson = facultyContactPersonRepository.findAllByFaculty(FacultyType.findById(facultyId));
                facultyContactPersonRepository.deleteAll(savedFacultyContactPerson);
            }
            facultyContactPersonRepository.saveAll(contactPeople);
            return facultyContactPersonRepository.findAllByFaculty(FacultyType.findById(facultyId));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


    public List<FacultyOpenHour> getOpeningHoursForFaculty(String login, Integer facultyId) {
        Document doc;
        User user = userService.getUserByLogin(login);
        try {
            doc = Jsoup.connect("https://is.stuba.sk/auth/pracoviste/stud_odd.pl")
                    .data("fakulta", facultyId.toString())
                    .cookie("UISAuth", user.getUis_auth())
                    .get();
            logger.info(doc.title());
            List<Node> nodes = doc.select("table").get(1).childNode(1).childNodes();
            List<FacultyOpenHour> openHours = new ArrayList<>();
            for (int i = 0; i < nodes.size(); i++) {
                FacultyOpenHour openHour = new FacultyOpenHour();
                openHour.setFaculty(FacultyType.findById(facultyId));
                List<Node> nodesInRow = nodes.get(i).childNodes();
                for (int j = 0; j < nodesInRow.size(); j++) {
                    String value = null;
                    try{
                         value = nodesInRow.get(j).childNode(0).childNode(0).toString().replace("\n", "");
                    } catch (Exception e) {

                    }

                    if (j == 0) {
                        openHour.setDayOfWeek(DayOfWeek.valueOf(value.toUpperCase()));
                    } else if (j == 1) {
                        openHour.setFrom(value);
                    } else if (j == 2) {
                        openHour.setTo(value);
                    } else if (j == 3) {
                        openHour.setNote(value);
                    } else {
                    }
                }
                openHours.add(openHour);
            }
            if (!openHours.isEmpty()) {
                List<FacultyOpenHour> savedOpenHours = facultyOpenHourRepository.findAllByFaculty(FacultyType.findById(facultyId));
                savedOpenHours.forEach(d -> facultyOpenHourRepository.delete(d));
            }
            facultyOpenHourRepository.saveAll(openHours);
            return facultyOpenHourRepository.findAllByFaculty(FacultyType.findById(facultyId));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}
