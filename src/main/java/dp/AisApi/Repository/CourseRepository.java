package dp.AisApi.Repository;

import dp.AisApi.Model.Course.Course;
import dp.AisApi.Model.SemesterType;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CourseRepository extends MongoRepository<Course, String> {

    public List<Course> findByLogin(String login);

    public Course findByLoginAndCourseNumberAndSemesterTypeAndPeriod(String login, String courseNumber,
                                                                      SemesterType type,String period);
}
