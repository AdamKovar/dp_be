package dp.AisApi.Repository;

import dp.AisApi.Model.Faculty.FacultyContactPerson;
import dp.AisApi.Model.Faculty.FacultyType;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface FacultyContactPersonRepository extends MongoRepository<FacultyContactPerson, String> {

public List<FacultyContactPerson> findAllByFaculty(FacultyType facultyType);

}
