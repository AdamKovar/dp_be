package dp.AisApi.Repository;

import dp.AisApi.Model.Faculty.FacultyContact;
import dp.AisApi.Model.Faculty.FacultyType;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface FacultyContactRepository extends MongoRepository<FacultyContact, String> {

    public FacultyContact findByFaculty(FacultyType facultyType);

}