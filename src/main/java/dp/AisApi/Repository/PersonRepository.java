package dp.AisApi.Repository;

import dp.AisApi.Model.Person.Person;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonRepository extends MongoRepository<Person, String> {

    public Person findByAisId(String aisId);

    public long countByAisId(String aisId);

}
