package dp.AisApi.Repository;

import dp.AisApi.Model.Event.TimeTableEvent;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TimeTableEventRepository extends MongoRepository<TimeTableEvent, String> {

    public List<TimeTableEvent> findByLogin(String login);

    TimeTableEvent findByLoginAndCourseNoAndEventTypeAndValidFromAndValidTo(String login, String courseNo, String eventType,
                                                                             String validFrom, String validTo);

}
