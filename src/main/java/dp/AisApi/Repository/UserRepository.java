package dp.AisApi.Repository;

import dp.AisApi.Model.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends MongoRepository<User, String> {

    public User findByLogin(String login);

    public User findByAisId(String aisId);

    public Long countByLogin(String login);

    public Long countByAisId(String aisId);
}
