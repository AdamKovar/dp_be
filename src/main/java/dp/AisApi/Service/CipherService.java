package dp.AisApi.Service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class CipherService {

    @Value( "${secret_key}" )
    String secretKey;

    @Value( "${secret_salt}" )
    String salt;

    public String encrypt(String value) {
     return AES.encrypt(value, secretKey, salt) ;
    }

    public String decrypt(String value) {
        return AES.decrypt(value, secretKey, salt) ;
    }

}
