package dp.AisApi.Service;

import dp.AisApi.Model.Email.EmailConfiguration;
import dp.AisApi.Model.Email.EmailMessage;
import dp.AisApi.Model.Email.EmailPreview;
import dp.AisApi.Model.Email.MailBoxStatus;

import javax.mail.*;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.IOException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EmailReceiver {


    private Properties getServerProperties(String protocol, String host,
                                           String port) {
        Properties properties = new Properties();

        // server setting
        properties.put(String.format("mail.%s.host", protocol), host);
        properties.put(String.format("mail.%s.port", protocol), port);

        // SSL setting
        properties.setProperty(
                String.format("mail.%s.socketFactory.class", protocol),
                "javax.net.ssl.SSLSocketFactory");
        properties.setProperty(
                String.format("mail.%s.socketFactory.fallback", protocol),
                "false");
        properties.setProperty(
                String.format("mail.%s.socketFactory.port", protocol),
                String.valueOf(port));

        return properties;
    }


    public EmailMessage getEmail(EmailConfiguration configuration, Integer index) {
        Properties properties = getServerProperties(configuration.getProtocol(),
                configuration.getHost(), configuration.getPort());
        Session session = Session.getDefaultInstance(properties);

        try {
            // connects to the message store
            Store store = session.getStore(configuration.getProtocol());
            store.connect(configuration.getUserName(), configuration.getPassword());

            // opens the inbox folder
            Folder folderInbox = store.getFolder("INBOX");
            folderInbox.open(Folder.READ_WRITE);

            // fetches new messages from server


            Message msg = folderInbox.getMessage(index);
            Address[] fromAddress = msg.getFrom();
            String from = fromAddress[0].toString();
            String subject = msg.getSubject();
            String toList = parseAddresses(msg
                    .getRecipients(MimeMessage.RecipientType.TO));
            String ccList = parseAddresses(msg
                    .getRecipients(MimeMessage.RecipientType.CC));


            String contentType = msg.getContentType();
            String messageContent = "";

            if (contentType.contains("text/plain")
                    || contentType.contains("text/html")) {
                try {
                    Object content = msg.getContent();
                    if (content != null) {
                        messageContent = content.toString();
                    }
                } catch (Exception ex) {
                    messageContent = "[Error downloading content]";
                    ex.printStackTrace();
                }
            } else {
                try {
                    MimeMultipart mimeMultipart = (MimeMultipart) msg.getContent();
                    messageContent = getTextFromMimeMultipart(mimeMultipart);
                } catch (Exception ex) {
                    messageContent = "[Error downloading content]";
                    ex.printStackTrace();
                }
            }

            EmailMessage emailMessage = new EmailMessage();
            emailMessage.setFrom(from);
            emailMessage.setTo(toList);
            emailMessage.setCc(ccList);
            emailMessage.setSubject(subject);
            String sentDate ;
            String sentTime ;
            String myDatePattern = "dd.MM.yyyy";
            String myTimePattern = "HH:ss";
            LocalDateTime dateTime = LocalDateTime.ofInstant(Instant.ofEpochMilli(msg.getSentDate().getTime()),
                    TimeZone.getDefault().toZoneId());
            DateTimeFormatter myDateFormatter = DateTimeFormatter.ofPattern(myDatePattern, Locale.ENGLISH);
            DateTimeFormatter myTimeFormatter = DateTimeFormatter.ofPattern(myTimePattern, Locale.ENGLISH);
            sentDate = dateTime.format(myDateFormatter);
            sentTime = dateTime.format(myTimeFormatter);
            emailMessage.setSentDate(sentDate);
            emailMessage.setSentTime(sentTime);
            emailMessage.setMessage(messageContent);

            msg.setFlag(Flags.Flag.SEEN, true);

            // disconnect
            folderInbox.close(false);
            store.close();

            return emailMessage;
        } catch (javax.mail.NoSuchProviderException e) {
            e.printStackTrace();
        } catch (javax.mail.MessagingException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<EmailPreview> downloadEmailPreview(EmailConfiguration configuration) {
        Properties properties = getServerProperties(configuration.getProtocol(),
                configuration.getHost(), configuration.getPort());
        Session session = Session.getDefaultInstance(properties);

        try {
            // connects to the message store
            Store store = session.getStore(configuration.getProtocol());
            store.connect(configuration.getUserName(), configuration.getPassword());

            // opens the inbox folder
            Folder folderInbox = store.getFolder("INBOX");
            folderInbox.open(Folder.READ_ONLY);

            // fetches new messages from server
            Message[] messages = folderInbox.getMessages();

            List<EmailPreview> emailPreviewList = new ArrayList<>();

            for (int i = 0; i < messages.length; i++) {
                EmailPreview preview = new EmailPreview();
                Message msg = messages[i];
                Address[] fromAddress = msg.getFrom();
                Matcher m = Pattern.compile("([a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\\.[a-zA-Z0-9_-]+)").matcher(fromAddress[0].toString());
                String from = "";
                while (m.find()) {
                    from = m.group();
                }
                String subject = msg.getSubject();
                String sentDate = msg.getSentDate().toString();


                preview.setId(msg.getMessageNumber());
                preview.setFrom(from);
                preview.setSentDate(sentDate);
                preview.setTitle(subject);

                Flags flags = msg.getFlags();
                if (isRead(flags)) {
                    preview.setStatus("SEE");
                } else {
                    preview.setStatus("UNREAD");
                }
                emailPreviewList.add(preview);
            }

            // disconnect
            folderInbox.close(false);
            store.close();
            return emailPreviewList;
        } catch (javax.mail.NoSuchProviderException e) {
            e.printStackTrace();
        } catch (javax.mail.MessagingException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<EmailPreview> downloadEmailPreviewByPage(EmailConfiguration configuration,
                                                         Integer statIndex, Integer endIndex) {
        Properties properties = getServerProperties(configuration.getProtocol(),
                configuration.getHost(), configuration.getPort());
        Session session = Session.getDefaultInstance(properties);

        try {
            // connects to the message store
            Store store = session.getStore(configuration.getProtocol());
            store.connect(configuration.getUserName(), configuration.getPassword());

            // opens the inbox folder
            Folder folderInbox = store.getFolder("INBOX");
            folderInbox.open(Folder.READ_ONLY);

            int count = folderInbox.getMessageCount();
            if (statIndex > count) {
                return new ArrayList<>();
            } else if (endIndex > count) {
                endIndex = count;
            }
            // fetches new messages from server
            Message[] messages = folderInbox.getMessages(statIndex, endIndex);

            List<EmailPreview> emailPreviewList = new ArrayList<>();

            for (int i = 0; i < messages.length; i++) {
                EmailPreview preview = new EmailPreview();
                Message msg = messages[i];
                Address[] fromAddress = msg.getFrom();
                Matcher m = Pattern.compile("([a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\\.[a-zA-Z0-9_-]+)").matcher(fromAddress[0].toString());
                String from = "";
                while (m.find()) {
                    from = m.group();
                }
                String subject = msg.getSubject();
                String sentDate ;
                String sentTime ;
                String myDatePattern = "dd.MM.yyyy";
                String myTimePattern = "HH:ss";
                LocalDateTime dateTime = LocalDateTime.ofInstant(Instant.ofEpochMilli(msg.getSentDate().getTime()),
                        TimeZone.getDefault().toZoneId());
                DateTimeFormatter myDateFormatter = DateTimeFormatter.ofPattern(myDatePattern, Locale.ENGLISH);
                DateTimeFormatter myTimeFormatter = DateTimeFormatter.ofPattern(myTimePattern, Locale.ENGLISH);
                sentDate = dateTime.format(myDateFormatter);
                sentTime = dateTime.format(myTimeFormatter);
                preview.setId(msg.getMessageNumber());


                preview.setFrom(from);


                preview.setSentDate(sentDate);
                preview.setSentTime(sentTime);
                preview.setTitle(subject);

                Flags flags = msg.getFlags();
                if (isRead(flags)) {
                    preview.setStatus("SEE");
                } else {
                    preview.setStatus("UNREAD");
                }
                emailPreviewList.add(preview);
            }

            // disconnect
            folderInbox.close(false);
            store.close();
            return emailPreviewList;
        } catch (NoSuchProviderException e) {
            e.printStackTrace();
        } catch (MessagingException e) {
            e.printStackTrace();
        }
        return null;
    }

    public MailBoxStatus getMailBoxStatus(EmailConfiguration configuration) {
        Properties properties = getServerProperties(configuration.getProtocol(),
                configuration.getHost(), configuration.getPort());
        Session session = Session.getDefaultInstance(properties);

        try {
            // connects to the message store
            Store store = session.getStore(configuration.getProtocol());
            store.connect(configuration.getUserName(), configuration.getPassword());

            // opens the inbox folder
            Folder folderInbox = store.getFolder("INBOX");
            folderInbox.open(Folder.READ_ONLY);

            MailBoxStatus status = new MailBoxStatus();
            status.setAllMessages(folderInbox.getMessageCount());
            status.setUnreadMessages(folderInbox.getUnreadMessageCount());

            folderInbox.close(false);
            store.close();
            return status;


        } catch (NoSuchProviderException e) {
            e.printStackTrace();
        } catch (MessagingException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void markEmailAsRead(EmailConfiguration configuration, Integer index) {
        Properties properties = getServerProperties(configuration.getProtocol(),
                configuration.getHost(), configuration.getPort());
        Session session = Session.getDefaultInstance(properties);

        try {
            // connects to the message store
            Store store = session.getStore(configuration.getProtocol());
            store.connect(configuration.getUserName(), configuration.getPassword());

            // opens the inbox folder
            Folder folderInbox = store.getFolder("INBOX");
            folderInbox.open(Folder.READ_WRITE);

            Message message = folderInbox.getMessage(index);
            message.setFlag(Flags.Flag.SEEN, true);

            folderInbox.close(false);
            store.close();

        } catch (NoSuchProviderException e) {
            e.printStackTrace();
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    private String parseAddresses(Address[] address) {
        String listAddress = "";

        if (address != null) {
            for (int i = 0; i < address.length; i++) {
                listAddress += address[i].toString() + ", ";
            }
        }
        if (listAddress.length() > 1) {
            listAddress = listAddress.substring(0, listAddress.length() - 2);
        }

        return listAddress;
    }

    private String getTextFromMimeMultipart(
            MimeMultipart mimeMultipart) {
        String result = "";
        try {
            int count = mimeMultipart.getCount();
            for (int i = 0; i < count; i++) {
                BodyPart bodyPart = mimeMultipart.getBodyPart(i);
                if (bodyPart.isMimeType("text/plain")) {
                    result = result + "\n" + bodyPart.getContent();
                    break; // without break same text appears twice in my tests
                } else if (bodyPart.isMimeType("text/html")) {
                    String html = (String) bodyPart.getContent();
                    result = result + "\n" + org.jsoup.Jsoup.parse(html).text();
                } else if (bodyPart.getContent() instanceof MimeMultipart) {
                    result = result + getTextFromMimeMultipart((MimeMultipart) bodyPart.getContent());
                }
            }
            return result;
        } catch (MessagingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    private boolean isRead(Flags flags) {
        for (Flags.Flag flag : flags.getSystemFlags()) {
            if (flag.equals(Flags.Flag.SEEN)) {
                return true;
            }
        }
        return false;
    }
}
