package dp.AisApi.Service;

import dp.AisApi.Model.Faculty.*;
import dp.AisApi.Parser.FacultyContactParser;
import dp.AisApi.Repository.FacultyContactPersonRepository;
import dp.AisApi.Repository.FacultyContactRepository;
import dp.AisApi.Repository.FacultyOpenHourRepository;
import dp.AisApi.Repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class FacultyContactService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private FacultyContactRepository facultyContactRepository;

    @Autowired
    private FacultyOpenHourRepository facultyOpenHourRepository;

    @Autowired
    private FacultyContactPersonRepository facultyContactPersonRepository;

    @Autowired
    private FacultyContactParser facultyContactParser;

    Logger logger = LoggerFactory.getLogger(FacultyContactService.class);

    public List<FacultyContact> getContactsForAllFaculties(String login) {
        List<FacultyContact> contacts = new ArrayList<>();
        for (FacultyType type : FacultyType.values()) {
            FacultyContact contact = facultyContactParser.getFacultyContact(login, type.getId());
            if (contact != null) {
                contacts.add(contact);
            }
        }
        return contacts;

    }

    public FacultyContact getFacultyContact(String login,Integer facultyId) {
        return facultyContactParser.getFacultyContact(login, facultyId);
    }

    public List<FacultyContactPerson> getFacultyContactPeopleForAllFaculties(String login) {
        List<FacultyContactPerson> facultyContactPeople = new ArrayList<>();
        for (FacultyType type : FacultyType.values()) {
            facultyContactPeople.addAll(facultyContactParser.getFacultyContactPeople(login, type.getId()));
        }
        return facultyContactPeople;
    }


    public List<FacultyContactPerson> getFacultyContactViceDeansForAllFaculties(String login) {
        List<FacultyContactPerson> facultyContactPeople = new ArrayList<>();
        for (FacultyType type : FacultyType.values()) {
            facultyContactPeople.addAll(facultyContactParser.getFacultyContactViceDeans(login, type.getId()));
        }
        return facultyContactPeople;
    }

    public List<FacultyOpenHour> getOpeningHoursForAllFaculties(String login) {
        List<FacultyOpenHour> facultyOpenHours = new ArrayList<>();
        for (FacultyType type : FacultyType.values()) {
            facultyOpenHours.addAll(facultyContactParser.getOpeningHoursForFaculty(login, type.getId()));
        }
        return facultyOpenHours;
    }

    public List<FacultyOpenHour> getOpeningHoursForFaculty(String login, Integer facultyId) {
       return facultyContactParser.getOpeningHoursForFaculty(login, facultyId);
    }

    public List<FacultyContactPerson> getFacultyContactPeople(String login, Integer facultyId) {
        return facultyContactParser.getFacultyContactPeople(login, facultyId);
    }

    public List<FacultyContactPerson> getFacultyContactViceDeans(String login, Integer facultyId) {
        return facultyContactParser.getFacultyContactViceDeans(login, facultyId);
    }

}
