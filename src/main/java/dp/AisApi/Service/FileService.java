package dp.AisApi.Service;

import dp.AisApi.Model.User;
import dp.AisApi.Repository.UserRepository;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Service
public class FileService {

    Logger logger = LoggerFactory.getLogger(FileService.class);

    @Autowired
    private UserRepository userRepository;

    //https://is.stuba.sk/auth/student/potvrzeni_o_studiu.pl?studium=144441&obdobi=557&login_hidden=1&auth_id_hidden=0&credential_0=xkovar&credential_1=Polovacka123&tisk=Tlačiť potvrdenie
    public void downloadData(String login) {
        User user = userRepository.findByLogin("xkovar");
        Map<String, String> data = new HashMap<>();
        data.put("tisk", "Tlačiť potvrdenie");
        data.put("studium", "144441");
        data.put("obdobi", "557");
        try {
            Document doc = Jsoup.connect("https://is.stuba.sk/auth/student/potvrzeni_o_studiu.pl?studium=144441&obdobi=557&login_hidden=1&auth_id_hidden=0&credential_0=xkovar&credential_1=Polovacka123&tisk=Tlačiť potvrdenie")
                    .get();
            logger.info(doc.title());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
