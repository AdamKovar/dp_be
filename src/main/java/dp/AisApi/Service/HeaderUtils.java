package dp.AisApi.Service;

import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

public class HeaderUtils {

    public HeaderUtils() {
    }

    public static HttpHeaders getResponseHeaders(String fileName, long length, MediaType mediaType) {
        HttpHeaders respHeaders = new HttpHeaders();
        respHeaders.setContentType(mediaType);
        respHeaders.setContentLength(length);
        respHeaders.setContentDispositionFormData("attachment", fileName);
        return respHeaders;
    }
}
