package dp.AisApi.Service;

import dp.AisApi.Model.Document.DocumentType;
import dp.AisApi.Model.Email.*;
import dp.AisApi.Model.Person.Person;
import dp.AisApi.Model.Person.PersonInfo;
import dp.AisApi.Model.User;
import dp.AisApi.Repository.PersonRepository;
import dp.AisApi.Repository.UserRepository;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

@Service
public class PersonService {

    Logger logger = LoggerFactory.getLogger(PersonService.class);

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private CipherService cipherService;

    public Person getPersonalInfo(String login) {
        Person person = new Person();
        User user = userService.getUserByLogin(login);
        logger.info(user.getLogin());
        logger.info(user.getUis_auth());
        logger.info(user.getAisId());
        try {
            Document doc = Jsoup.connect("https://is.stuba.sk/auth/kontrola?")
                    .cookie("UISAuth", user.getUis_auth())
                    .get();
            logger.info(doc.title());
            Elements elements = doc.select("table#tmtab_1").select("tr");
            for (int i = 1; i < elements.size() - 2; i++) {
                if (elements.get(i).childNodeSize() > 0) {
                    try {
                        String key = elements.get(i).select("td").get(0).childNode(0).childNode(0).toString().replace("&nbsp;", " ").replace("\n","");
                        String value = elements.get(i).select("td").get(1).childNode(0).childNode(0).toString().replace("&nbsp;", " ").replace("\n","");
                        switch (key) {
                            case "First name and surname":
                                person.setName(value);
                                break;
                            case "Identification number":
                                person.setAisId(value);
                                break;
                            case "Date of birth":
                                person.setDateOfBirth(value);
                                break;
                            case "Sex":
                                person.setGender(value);
                                break;
                            case "Degree before the name":
                                person.setTitle(value);
                                break;
                            case "E-mail":
                                person.setMail(value);
                                break;
                            default:
                                if (i == elements.size()) {
                                    System.out.println();
                                }
                                break;
                        }
                    } catch (Exception e) {
                        continue;
                    }

                }
            }
            person.setLogin(login);
            if (personRepository.countByAisId(person.getAisId()) <= 0) {
                personRepository.save(person);
            } else {
                Person savedPerson = personRepository.findByAisId(person.getAisId());
                savedPerson.update(person);
                personRepository.save(savedPerson);
            }
            user.setAisId(person.getAisId());
            userService.save(user);
            return personRepository.findByAisId(person.getAisId());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<PersonInfo> getAllPersonalInfo(String login) {
        Person person = new Person();
        User user = userService.getUserByLogin(login);
        logger.info(user.getLogin());
        logger.info(user.getUis_auth());
        logger.info(user.getAisId());
        try {
            Document doc = Jsoup.connect("https://is.stuba.sk/auth/kontrola?;lang=sk")
                    .cookie("UISAuth", user.getUis_auth())
                    .get();
            logger.info(doc.title());
            List<PersonInfo> personalInfo = new ArrayList<>();
            Elements elements = doc.select("table#tmtab_1").select("tr");
            for (int i = 1; i < elements.size() - 2; i++) {
                if (elements.get(i).childNodeSize() > 0) {
                    try {
                        PersonInfo info = new PersonInfo();
                        info.setLabel(elements.get(i).select("td").get(0).childNode(0).childNode(0).toString().replace("&nbsp;", " ").replace("\n", ""));
                        info.setValue(elements.get(i).select("td").get(1).childNode(0).childNode(0).toString().replace("&nbsp;", " ").replace("\n", ""));
                        personalInfo.add(info);

                    } catch (Exception e) {
                        continue;
                    }

                }
            }
            return personalInfo;
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return null;
    }

    public byte[] getImage(String login, String aisId) {
        User user = userService.getUserByLogin(login);
        try {
            HttpGet get = new HttpGet(createImageUrl(login, aisId));
            get.setHeader("Content-Type", "image/jpeg");
            get.setHeader("Access-Control-Allow-Origin", "https://localhost.that.never.exists/");
            HttpResponse resp = null;
            try {
                HttpClient client = HttpClientBuilder.create().build();
                resp = client.execute(get);

                InputStream is = resp.getEntity().getContent();

                ByteArrayOutputStream buffer = new ByteArrayOutputStream();

                int read = 0;
                byte[] data = new byte[58383];

                while ((read = is.read(data)) > 0) {
                    buffer.write(data, 0, read);
                }

                is.close();
                return buffer.toByteArray();

            } catch (ClientProtocolException e) {

            } catch (IOException e) {

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private String createImageUrl(String login, String aisId) {
        User user = userService.getUserByLogin(login);
        String url = "https://is.stuba.sk/auth/lide/foto.pl?login_hidden=1&auth_id_hidden=0" +
                "&credential_0=#LOGIN#&credential_1=#PASSWORD#&id=#AISID#";
        url = url.replace("#PASSWORD#", cipherService.decrypt(user.getPassword()));
        url = url.replace("#LOGIN#", user.getLogin());
        url = url.replace("#AISID#", aisId);
        return url;
    }

    public List<EmailPreview> getMailPreview(String login, InboxType inboxType) {
        User user = userService.getUserByLogin(login);
        EmailConfiguration configuration = EmailConfiguration.from(user.getLogin(),
                cipherService.decrypt(user.getPassword()), inboxType);

        EmailReceiver receiver = new EmailReceiver();
        return receiver.downloadEmailPreview(configuration);
    }

    public List<EmailPreview> getMailPreview(String login, InboxType inboxType, Integer startIndex, Integer endIndex) {
        User user = userService.getUserByLogin(login);
        EmailConfiguration configuration = EmailConfiguration.from(user.getLogin(),
                cipherService.decrypt(user.getPassword()), inboxType);

        EmailReceiver receiver = new EmailReceiver();
        return receiver.downloadEmailPreviewByPage(configuration, startIndex, endIndex);
    }

    public EmailMessage getEmail(String login, Integer index, InboxType inboxType) {
        User user = userService.getUserByLogin(login);
        EmailConfiguration configuration = EmailConfiguration.from(user.getLogin(),
                cipherService.decrypt(user.getPassword()), inboxType);

        EmailReceiver receiver = new EmailReceiver();
        return receiver.getEmail(configuration, index);
    }

    public MailBoxStatus getMailBoxStatus(String login, InboxType inboxType) {
        User user = userService.getUserByLogin(login);
        EmailConfiguration configuration = EmailConfiguration.from(user.getLogin(),
                cipherService.decrypt(user.getPassword()), inboxType);

        EmailReceiver receiver = new EmailReceiver();
        return receiver.getMailBoxStatus(configuration);
    }

    public void markEmailAsRead(String login, Integer index, InboxType inboxType) {
        User user = userService.getUserByLogin(login);
        EmailConfiguration configuration = EmailConfiguration.from(user.getLogin(),
                cipherService.decrypt(user.getPassword()), inboxType);

        EmailReceiver receiver = new EmailReceiver();
        receiver.markEmailAsRead(configuration, index);
    }

}
