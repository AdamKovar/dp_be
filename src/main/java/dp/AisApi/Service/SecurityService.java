package dp.AisApi.Service;

import dp.AisApi.Model.Person.Person;
import dp.AisApi.Model.User;
import dp.AisApi.Repository.UserRepository;
import lombok.AllArgsConstructor;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Map;

@Service
public class SecurityService {

    Logger logger = LoggerFactory.getLogger(SecurityService.class);

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private CipherService cipherService;

    @Autowired
    private PersonService personService;

    public boolean logIn(String login, String password) {
        try {
            Connection.Response response = Jsoup.connect("https://is.stuba.sk/auth/student/moje_studium.pl?_m=3110;lang=sk&" +
                    "login_hidden=1&auth_id_hidden=0&credential_0=" + login + "&credential_1=" + password)
                    .method(Connection.Method.GET)
                    .execute();
            Map<String, String> cookies = response.cookies();
            String authHash = cookies.get("UISAuth");
            if (!StringUtils.isEmpty(authHash)) {
                Document homePage = Jsoup.connect("https://is.stuba.sk/auth/student/moje_studium.pl?_m=3110;lang=en")
                        .cookie("UISAuth", authHash)
                        .get();
                logger.info(homePage.title());

                if (userRepository.countByLogin(login) <= 0) {
                    User user = new User(login, authHash, "");
                    setStudyAndPeriod(homePage, user);
                    user.setPassword(cipherService.encrypt(password));
                    userService.save(user);
                } else {
                    User user = userRepository.findByLogin(login);
                    user.setUis_auth(authHash);
                    setStudyAndPeriod(homePage, user);
                    user.setPassword(cipherService.encrypt(password));
                    userService.save(user);
                }
                personService.getPersonalInfo(login);
                logger.info("AUTH SUCCESS");
                return true;

            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }


    private void setStudyAndPeriod(Document homePage, User user) {
        Elements elements = homePage.select("a[href]");
        for (int i = 0; i < elements.size(); i++) {
            if (elements.get(i).attr("href").contains("studium")) {
                for (String s : homePage.select("a[href]").attr("href").split(";")) {
                    if (s.contains("obdobi")) {
                        user.setCurrentPeriod(s.split("=")[1]);
                    }
                    if (s.contains("studium")) {
                        user.setStudyNumber(s.split("=")[1]);
                    }
                }
                return;
            }
        }
    }

}
