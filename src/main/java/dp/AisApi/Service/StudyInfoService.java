package dp.AisApi.Service;

import dp.AisApi.Model.Course.Course;
import dp.AisApi.Model.Course.CourseType;
import dp.AisApi.Model.Course.ExamMark;
import dp.AisApi.Model.Course.ExamType;
import dp.AisApi.Model.Document.DocumentType;
import dp.AisApi.Model.SemesterType;
import dp.AisApi.Model.User;
import dp.AisApi.Repository.CourseRepository;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class StudyInfoService {

    Logger logger = LoggerFactory.getLogger(StudyInfoService.class);

    @Autowired
    private CourseRepository courseRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private CipherService cipherService;


    public List<Course> getStudyInfo(String login) {
        Document doc;
        User user = userService.getUserByLogin(login);
        try {
            doc = Jsoup.connect("https://is.stuba.sk/auth/student/pruchod_studiem.pl")
                    .cookie("UISAuth", user.getUis_auth())
                    .data("vyber", "vsechna_obdobi")
                    .data("lang", "sk")
                    .get();
            logger.info(doc.title());
            Elements formElements = doc.select("form").get(0).children();
            String period = "";
            List<Course> courses = new ArrayList<>();
            for (int elementIndex = 0; elementIndex < formElements.size(); elementIndex++) {
                Element element = formElements.get(elementIndex);
                if (element.tagName().equals("b") && element.childNode(0).toString().contains("/")) {
                    period = element.childNode(0).toString().replace("\n", "");
                }
                if (element.id().contains("tmtab")) {
                    List<Node> subjects = element.childNode(1).childNodes();
                    courses.addAll(parseCourseInfo(period, subjects, user));
                }
            }
            return courses;
            //return courseRepository.findByUserId(user.getAisId());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Collections.emptyList();
    }

    private List<Course> parseCourseInfo(String period, List<Node> subjects, User user) {
        List<Course> courses = new ArrayList<>();
        for (int j = 0; j < subjects.size(); j++) {
            List<Node> elements = subjects.get(j).childNodes();
            String name = "";
            String reference = "";
            String courseNumber = "";
            CourseType type = CourseType.COURSE_TYPE_P;
            String language = "";
            ExamType examType = ExamType.EXAM_TYPE_EXM;
            String examAttempt = "";
            ExamMark examMark = ExamMark.EXAM_MARK_NONE;
            String credits = "";
            SemesterType semesterType = SemesterType.SEMESTER_TYPE_WS;
            String faculty = "";

            for (int k = 0; k < elements.size(); k++) {

                Node node = elements.get(k).childNode(0);
                switch (k) {
                    case 0:
                        try {
                            name = node.childNode(0).childNode(0).toString().replace("\n", "").replace("&nbsp;","");
                            reference = node.childNode(0).childNode(0).attr("href").toString().replace("\n", "");
                            courseNumber = node.childNode(0).attr("href").split("=")[1].replace("\n", "");
                        } catch (Exception e) {

                        }
                        break;
                    case 1:
                        try {
                            type = CourseType.getByType(node.childNode(0).attr("alt").replace("\n", ""));
                        } catch (Exception e) {

                        }
                        break;
                    case 2:
                        try {
                            language = node.childNode(0).toString().replace("\n", "");
                        } catch (Exception e) {

                        }
                        break;
                    case 3:
                        try {
                            examType = ExamType.getByName(node.childNode(0).toString().replace("\n", ""));
                        } catch (Exception e) {

                        }
                        break;
                    case 4:
                        try {
                            examAttempt = node.childNode(0).childNode(0).toString().replace("\n", "");
                        } catch (Exception e) {

                        }
                        break;
                    case 5:
                        try {
                            examMark = ExamMark.getMark(node.childNode(1).toString().split(";")[1].replace("\n", ""));
                        } catch (Exception e) {

                        }
                        break;
                    case 6:
                        try {
                            credits = node.childNode(0).toString().replace("\n", "");
                        } catch (Exception e) {

                        }
                        break;
                    default:
                        break;
                }
            }
            semesterType = SemesterType.findByAcronym(period.split(" ")[0].trim());
            faculty = period.split("-")[1].trim().replace(":", "");
            Course course = new Course(name, reference, courseNumber, type, language,
                    examType, examAttempt, examMark, credits, semesterType,
                    period, faculty, user.getLogin());
            createOrUpdateInfo(course);
            courses.add(course);
        }
        return courses;
    }

    private void createOrUpdateInfo(Course course) {
        Course savedCourse = courseRepository.findByLoginAndCourseNumberAndSemesterTypeAndPeriod(course.getLogin(),
                course.getCourseNumber(), course.getSemesterType(), course.getPeriod());
        if (savedCourse != null) {
            savedCourse.update(savedCourse, course.getExamMark(), course.getExamAttempt());
        } else {
            courseRepository.save(course);
        }
    }

    public byte[] getPdf(String login, String docType) {
        User user = userService.getUserByLogin(login);
        try {
            HttpGet get = new HttpGet(createConnectionUrl(user, DocumentType.valueOf(docType)));
            get.setHeader("Content-Type", "application/pdf");
            get.setHeader("Access-Control-Allow-Origin", "https://localhost.that.never.exists/");
            get.setHeader("Content-Disposition", "attachment; filename=file.pdf");
            HttpResponse resp = null;
            try {
                HttpClient client = HttpClientBuilder.create().build();
                resp = client.execute(get);

                InputStream is = resp.getEntity().getContent();

                ByteArrayOutputStream buffer = new ByteArrayOutputStream();

                int read = 0;
                byte[] data = new byte[58383];

                while ((read = is.read(data)) > 0) {
                    buffer.write(data, 0, read);
                }

                is.close();
                return buffer.toByteArray();

            } catch (ClientProtocolException e) {

            } catch (IOException e) {

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private String createConnectionUrl(User user, DocumentType docType) {
        String url = docType.getUrl();
        url = url.replace("#STUDY_NO#", user.getStudyNumber());
        url = url.replace("#PERIOD_NO#", user.getCurrentPeriod());
        url = url.replace("#PASSWORD#", cipherService.decrypt(user.getPassword()));
        url = url.replace("#LOGIN#", user.getLogin());


        return url;

    }


}
