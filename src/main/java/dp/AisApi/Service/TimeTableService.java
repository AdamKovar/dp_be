package dp.AisApi.Service;

import dp.AisApi.Model.DayOfWeek;
import dp.AisApi.Model.Event.TimeTableEvent;
import dp.AisApi.Model.User;
import dp.AisApi.Repository.TimeTableEventRepository;
import dp.AisApi.Repository.UserRepository;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.TextNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TimeTableService {

    Logger logger = LoggerFactory.getLogger(SecurityService.class);

    @Autowired
    private TimeTableEventRepository timeTableEventRepository;

    @Autowired
    private UserService userService;

    public List<TimeTableEvent> getTimeTable(String login) {
        Document doc;
        User user = userService.getUserByLogin(login);
        try {
            doc = Jsoup.connect("https://is.stuba.sk/auth/katalog/rozvrhy_view.pl")
                    .cookie("UISAuth", user.getUis_auth())
                    .data("rozvrh_student_obec", "1")
                    .data("zobraz", "1")
                    .data("format","list")
                    .data("lang","sk")
                    .data("rozvrh_student",user.getAisId())
                    .get();
            logger.info(doc.title());
            String validateFrom = "";
            String validateTo = "";
            try{
                String[] timetableValidation = doc.select("p").get(2).parent().childNodes().stream()
                        .filter(n -> n instanceof TextNode).map(n->n.toString())
                        .filter(t -> t.contains("Valid"))
                        .collect(Collectors.toList()).get(0).split(":")[1].split("-");
                validateFrom = timetableValidation[0];
                validateTo = timetableValidation[1];
            }
            catch (Exception e){

            }
            try{
                String[] timetableValidation = doc.select("p").get(2).parent().childNodes().stream()
                            .filter(n -> n instanceof TextNode).map(n->n.toString())
                            .filter(t -> t.contains("Platnosť"))
                            .collect(Collectors.toList()).get(0).split(":")[1].split("-");

                validateFrom = timetableValidation[0];
                validateTo = timetableValidation[1];
            }
            catch (Exception e){

            }
            try {
                List<Node> subjects = doc.select("table").first().childNode(1).childNodes();
                for (Node node : subjects) {
                    TimeTableEvent event = new TimeTableEvent();
                    List<Node> elements = node.childNodes();
                    for (int i = 0; i < elements.size(); i++) {
                        if (i == 0) {
                            String dayOfWeek = elements.get(i).childNode(0).childNode(0).toString().replace("\n", "");
                            event.setDay(DayOfWeek.geDayOfWeekByLabel(dayOfWeek));
                        } else if (i == 1) {
                            event.setStart(elements.get(i).childNode(0).childNode(0).toString().replace("\n", ""));
                        } else if (i == 2) {
                            event.setEnd(elements.get(i).childNode(0).childNode(0).toString().replace("\n", ""));
                        } else if (i == 3) {
                            event.setName(elements.get(i).childNode(0).childNode(0).childNode(0).toString().replace("&nbsp;", " ").replace("\n", ""));
                            event.setCourseNo(elements.get(i).childNode(0).childNode(0).attr("href").split(";")[0].split("predmet=")[1].replace("\n", ""));
                        } else if (i == 4) {
                            event.setEventType(elements.get(i).childNode(0).childNode(0).toString().replace("\n", ""));
                        } else if (i == 5) {
                            event.setRoom(elements.get(i).childNode(0).childNode(0).childNode(0).toString().split("&")[0].replace("\n", ""));
                        } else if (i == 6) {
                            event.setTeacher(elements.get(i).childNode(0).childNode(0).childNode(0).childNode(0).toString().replace("\n", ""));
                        }
                    }
                    event.setValidFrom(validateFrom);
                    event.setValidTo(validateTo);
                    event.setLogin(user.getLogin());
                    createOrUpdateInfo(event);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return timeTableEventRepository.findByLogin(user.getLogin());
    }

    private void createOrUpdateInfo(TimeTableEvent event) {
        TimeTableEvent savedEvent = timeTableEventRepository.findByLoginAndCourseNoAndEventTypeAndValidFromAndValidTo(event.getLogin(),
                event.getCourseNo(), event.getEventType(), event.getValidFrom(), event.getValidTo());
        if (savedEvent != null) {

        } else {
            timeTableEventRepository.save(event);
        }
    }
}
