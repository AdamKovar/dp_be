package dp.AisApi.Service;

import dp.AisApi.Model.User;
import dp.AisApi.Repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class UserService {


    @Autowired
    private UserRepository userRepository;

    @Autowired
    private SecurityService securityService;

    @Autowired
    private CipherService cipherService;

    public User getUserByLogin(String login) {
        User user =  userRepository.findByLogin(login);
        if(LocalDateTime.now().minusMinutes(10).isAfter(user.getLastUpdateTime())) {
            securityService.logIn(user.getLogin(),cipherService.decrypt(user.getPassword()));
        }
        return userRepository.findByLogin(login);
    }

    public User save (User user) {
        user.setLastUpdateTime(LocalDateTime.now());
        return userRepository.save(user);
    }

}
